# Chef d'œuvre de M2 DC (2017-2018, Université Paul Sabatier)

Groupe de Guilhem Marcillaud, Charlotte Møller, Mengmeng Qi et Damien Wojtowicz.

# Prérequis

Le bon fonctionnement de notre programme requiert l'installation de la dernière version du langage Python (3.6), et des librairies Scikit-learn, graphviz, NLTK et le FrenchStemmer, Keras paramétré pour utiliser Theano (à installer aussi), pandas, matplotlib, pyLDAvis, gensim, tweepy.

# Guide d'utilisation

## Tâche 2

L'exécution du programme de détection de thèmes se fait de la sorte :

```shell
python3 tache2/clustering.py CORPUS
```

Le fichier CORPUS doit respecter le format du fichier `Tache2_train.csv` de Moodle. Le programme affiche sur la sortie standard les thèmes détectés : une ligne par thème, vingt mots maximum par thème séparés par un espace.

## Tâche 3

Here is a list of the names of python scripts used during the process of extracting communities with the input of a list of tweets.

### Data extraction

`get_users.py`: extracting the userIds of the people having emitted the tweets and the hashtag used for the tweet	


`get_FollowersAndFriends.py`: collecting all the edges of the graph: going through all users and save all connections to other users having emitted a 										   tweet in the list of tweets. The output is a file containing all edges in a directed graph

`community.py`: use igraph to calculate the cluster of communities using a method that aims at maximizing the modularity score. As output, 										 a file containing on each line a userId and it's assigned cluster


### Validating result

`get_statuses.py` : extracting the userIds of the people having emitted the tweets, the text of the tweet and the hashtag used for the tweet	

`get_textes.py`: extracting the tweets emitted within a cluster and put them in seperate file. The file format is adapted to fit into the 									   LDA of tache 2: in 3 columns, the two first are non important (here, we just put an "a"), the third is the tweet. 

`makeLastCSV.py`: fusion and formatting of different files to be able to read and visualize the result in Gephi

## Tâche 4.1

L'exécution du programme de détection de sentiment:

```shell
python3 tache4-1/classification_nb.py CORPUS_TRAIN CORPUS_TEST


python3 tache4-1/classification_cbow.py CORPUS_TRAIN CORPUS_TEST


python3 tache4-1/classification_nn.py CORPUS_TRAIN CORPUS_TEST
```

Les fichiers CORPUS_TRAIN et CORPUS_TEST doivent respecter le format du fichier `Tache4-1_train.csv` de Moodle.

## Tâche 4.2

### Arbres de décision

L'exécution du programme de détection d'ironie se fait de la sorte :

```shell
python3 tache4-2/tree.py CORPUS_TRAIN CORPUS_TEST


python3 tache4-2/randomForest.py CORPUS_TRAIN CORPUS_TEST


python3 tache4-2/extraTrees.py CORPUS_TRAIN CORPUS_TEST
```
Les fichiers CORPUS_TRAIN et CORPUS_TEST doivent respecter le format du fichier `Tache4-2_train.csv` de Moodle.

### Réseaux de neurones

L'exécution du programme de détection d'ironie se fait de la sorte :

```shell
python3 tache4-2/predire_neurones.py CORPUS
```

Le fichier CORPUS doit respecter le format du fichier `Tache4-2_train.csv` de Moodle, la colonne des annotations pouvant être ommise. Le programme affiche sur la sortie standard une ligne par tweet de la forme `ID <tabulation> polarité` ou la polarité est "figurative" ou "nonfigurative".

Un réseau de neurones préentraîné existe déjà. Il est possible d'entraîner un nouveau réseau avec la commande suivante :

```shell
python3 tache4-2/neurones.py CORPUS
```

Le format du fichier CORPUS devra respecter le format du fichier `Tache4-2_train.csv` de Moodle. La liste des options utilisables dans ce programme est accessible avec la commande :

```shell
python3 tache4-2/neurones.py -h
```