#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
"""Détection de thèmes les plus discutés dans le corpus de tweets

Propose une analyse de corpus avec les algorithmes LDA et LSA.
"""