#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Implémentation de la détection de thèmes par partitionnement.

L'algorithme utilisé est DBSCAN, prenant en paramètre les tweets sous forme de vecteurs TFIDF.
Les sujets sont les clusters retournés par DBSCAN, les mots caractéristiques sont des unigrammes,
bigrammes ou trigrammes les plus significatifs du cluster.

Référence pour DBSCAN :
Ester M, Kriegel HP, Sander J, Xu X. 
A density-based algorithm for discovering clusters in large spatial databases with noise. 
In Kdd 1996 Aug 2 (Vol. 96, No. 34, pp. 226-231).
http://www.aaai.org/Papers/KDD/1996/KDD96-037.pdf
"""
import sys
import argparse
import pprint

import lecture

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cluster import DBSCAN
from nltk.probability import FreqDist

def charger_corpus(corpus, est_propre):
	"""Charge le corpus.

	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper

	@param est_propre Le corpus a déjà subi un prétraitement.
		Type : bool

	@return Le corpus au comme étant une liste de documents.
		Type : [str]
	"""
	if est_propre :
		return [line for line in corpus]
	else :
		return [" ".join(doc) for doc in lecture.lecture(corpus)]


def afficher_cluster(clu, n_mots_sigs=20):
	"""Affiche les termes les plus significatifs d'un cluster.

	@param clu Le cluster
		Type : [[str]]

	@param n_mots_sigs Le nombre de mots caractérisant le cluster
		Type : int, > 0
	"""

	# Calcul de la matrice TF*IDF du cluster
	tf_clu = CountVectorizer(analyzer='word', min_df = 0)
	tfidf_matrix =  tf_clu.fit_transform(clu)
	feature_names = tf_clu.get_feature_names() 

	# Calcul des mots les plus significatifs
	dense = tfidf_matrix.todense()
	episode = dense[0].tolist()[0]
	mot_scores = [pair for pair in zip(range(0, len(episode)), episode) if pair[1] > 0]
	mot_scores = sorted(mot_scores, reverse=True)

	mot_scores = [feature_names[word_id] for (word_id, score) in mot_scores]
	print(" ".join(mot_scores[:n_mots_sigs]))


def __main__(args) :

	# Récupération du corpus et suppression des tweets vides
	in_corp = charger_corpus(args['corpus'], args['clean'])
	corpus = [el for el in in_corp if all(el)]

	# Récupération de la liste de tout les mots
	tout_mots = set([mt for tw in corpus for mt in tw.split()])

	# Calcul des vecteurs TF*IFD
	vectorizer = TfidfVectorizer(vocabulary=tout_mots)
	matrix = vectorizer.fit_transform(corpus)

	eps = 1.1
	# Clustering avec DBSCAN
	if len(corpus) < 500 :
		min_samples = 2
		eps = 1.2
	elif len(corpus) < 1000 :
		min_samples = 2
	elif len(corpus) < 2000 :
		min_samples = 3
	elif len(corpus) < 5000 :
		min_samples = 4
	elif len(corpus) < 10000 :
		min_samples = 5
	else :
		min_samples = 5
	
	c = DBSCAN(eps=eps, min_samples=min_samples, n_jobs=3)
	clu = c.fit_predict(matrix)
	n_clusters = len(set(c.labels_)) - (1 if -1 in c.labels_ else 0)

	# Récupération des clusters
	tw_clu = [[] for i in range(n_clusters)]
	for i in range(len(clu)) :
		if clu[i] != -1 :
			tw_clu[clu[i]].append(corpus[i])

	# Affichage des topics
	for clu in tw_clu :
		afficher_cluster(clu)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 clustering.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')

	parser.add_argument('--clean', 
		help="Précise si le corpus en entrée est le résultat des prétraitements de lecture.py.", 
		action="store_true")

	__main__(vars(parser.parse_args()))