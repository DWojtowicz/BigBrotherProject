#!/usr/bin/env python3
# -*- coding: utf8 -*-

import sys
import argparse
import lecture
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import pyLDAvis, pyLDAvis.sklearn


def charger_corpus(corpus, est_propore):
	"""Charge le corpus.

	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper

	@param est_propre Le corpus a déjà subi un prétraitement.
		Type : bool

	@return Le corpus au comme étant une liste de documents.
		Type : [str]
	"""
	if est_propore :
		return [line for line in corpus]
	else :
		return [" ".join(doc) for doc in lecture.lecture(corpus)]



def lda(corpus,top_n, min_df=2, max_df=0.5) :
	"""Implémentation de l'algorithme LDA.

	@param corpus Le corpus de documents sous forme de liste de tweets.
		Type : [str]

	@param top_n Le nombre de mots à utiliser pour définir un sujet.
		Type : int,  > 0

	@param min_df Seuil minimal de DF d'un mot.
		Type : int
		Défaut : 1

	@param max_df Le ratio maximal du nombre de documents dans lequel un mot peut apparaître.
		Type : float
		Défaut : 0.5

	@return Le liste des `nb_sujets` et la liste de perplexity associe
		Type : [[int],[float]]

	"""
	lda_models = []
	n_topics = list(range(25, 350,50 ))
	perplexityLst = [1.0]*len(n_topics)
	# Calcul des TFxIDF

	vectorizer = TfidfVectorizer(min_df=min_df, max_df=max_df)
	trans = vectorizer.fit_transform(corpus)

	for idx, n_topic in enumerate(n_topics):
		# Recherche des sujets, applique l'algorithme de Latent Dirichlet Allocation
		lda = LatentDirichletAllocation(n_components=n_topic, max_iter=1000,
                                learning_method='batch',
                                evaluate_every=200, 
                                perp_tol=0.01)
		lda.fit(trans)

		#visualisation du distribution des topics
		#vis = pyLDAvis.sklearn.prepare(lda, trans, vectorizer,R = top_n)
		#pyLDAvis.show(vis)

		#print la topics
		terms = vectorizer.get_feature_names()
		topics = []
		for i, comp in enumerate(lda.components_) :
			topics.append(sorted(zip(terms, comp), key=lambda x: x[1], reverse=True)[:top_n])

		for topic in topics :
			print("\t".join([x[0] for x in topic]))
	
		perplexityLst[idx] = lda.perplexity(trans)
		lda_models.append(lda)
		print("# of topics", n_topics[idx])
		print("Perplexity Score", perplexityLst[idx])

	return [n_topics,perplexityLst]

def plotCourbe(n_topics,perplexityLst):
	"""	
	@param n_topics la liste de nombre de topics.
		Type:[int]
	@param perplexityLst la list de perplexity associe a chaque nombre de topics
		Type:[float]
		"""
	# plot la courbe de nombre de sujet and la perplexity   
	fig = plt.figure()
	ax = fig.add_subplot(1,1,1)
	ax.plot(n_topics, perplexityLst)
	ax.set_xlabel("# of topics")
	ax.set_ylabel("Approximate Perplexity")
	plt.grid(True)
	plt.show()


def __main__(args) :

	corp = charger_corpus(args['corpus'], args['clean'])

	[n_topics,perplexityLst] = lda(corp,args['top_n'])
	plotCourbe(n_topics, perplexityLst)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 lda.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help='Le corpus de tweets en sortie de pretraitement.py')

	parser.add_argument('--top_n', type=int, 
		help='Le nombre de mots définissant un thème.', 
		default=6)

	parser.add_argument('--clean', 
		help="Précise si le corpus en entrée est le résultat des prétraitements de lecture.py.", 
		action="store_true")

	__main__(vars(parser.parse_args()))