import sys
import argparse
import lecture
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from gensim import corpora, models,matutils
from gensim.corpora.dictionary import Dictionary
from gensim.models.ldamodel import LdaModel
from gensim.models.hdpmodel import HdpModel

from collections import defaultdict 

def charger_corpus(corpus, est_propore):
	"""Charge le corpus.

	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper

	@param est_propre Le corpus a déjà subi un prétraitement.
		Type : bool

	@return Le corpus au comme étant une liste de documents.
		Type : [str]
	"""
	if est_propore :
		return [line for line in corpus]
	else :
		return [" ".join(doc) for doc in lecture.lecture(corpus)]

def print_topics(lda,vocab,top_n):
	topics = lda.show_topics(topics=-1,top_n = top_n,formatted=False)
	for terms,topic in enumerate(topics):
		print('topic %d: %s' % (ti, ' '.join('%s/%.2f' % (t[1], t[0]) for t in topic)))

def lda(corpus,top_n, min_df=1, max_df=0.5):
	nb_topics = 20

	lst_tws = [line.split() for line in corpus]
	dictionary = Dictionary(lst_tws)	
	corpus = [dictionary.doc2bow(tw) for tw in lst_tws]

	hdpmodel = HdpModel(corpus =  corpus,id2word = dictionary)

	ldamodel = LdaModel(corpus, num_topics=3, id2word = dictionary,passes=60)
	topics = []
	for i in range(nb_topics):
		topics.append(lda.get_topic_terms(i, topn=top_n))

	return topics



def __main__(args) :

	corp = charger_corpus(args['corpus'], args['clean'])

	topics = lda(corp,args['top_n'])
	print(topics)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 lda.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help='Le corpus de tweets en sortie de pretraitement.py')


	parser.add_argument('--top_n', type=int, 
		help='Le nombre de mots définissant un thème.', 
		default=10)

	parser.add_argument('--clean', 
		help="Précise si le corpus en entrée est le résultat des prétraitements de lecture.py.", 
		action="store_true")

	__main__(vars(parser.parse_args()))






