#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Prétraitements pour le corpus de tweets.

Il s'agit d'un script pouvant être exécuté à part (voir documentation dans le terminal : python3 lecture.py -h).
La fonction `lecture` qu'il propose peut être embarquée dans du code, de sorte à récupérer une liste de tweets, 
étant eux-mêmes des listes de chaines de caractères représentant les mots du corpus nettoyés et lemmatisés.
"""

import sys
import argparse
from pretraitement import mot_acceptable
from pretraitement import traitement_mot


def lecture(fic, hashtags=False, radic=False) :
	"""Lit le contenu du fichier de tweets et effectue la tokenisation.
	@param fic Le fichier CSV de tweets. Du format 'ID\tTAG\tTWEET'. 
		Type : TextIOWrapper
	@param hashtags Indique si les hashtags doivent rester intacts ou non. 
		Type : bool
		Défaut : False
	@param radic Indique si les mots doivent être radicalisés ou non. 
		Type : bool
		Défaut : False
	@return La liste des tweets. 
		Type : [str]
	"""
	tweets = []
	tmp = None

	# Lecture des lignes contenant des tweets
	for ligne in fic:
		if not (ligne.startswith("//") or ligne.startswith("/⁠/⁠")) and len(ligne) > 1:
			
			tmp = []
			for word in ligne.split('\t')[2].split():
				
				# Filtrage des mots vides et des "impuretés"
				if mot_acceptable(word.strip().lower()) :
					word = traitement_mot(word.strip(), hashtags, radic)

					# On vérifie à nouveau si le mot est OK
					# car des mots vides ressortent parfois du traitement
					if len(word) > 1 and mot_acceptable(word) :
						tmp.append(word)

			if tmp != [] :
				tweets.append(tmp)

	# Construction du corpus TFxIDF gensim	
	return tweets


def __ecrire_tws(tws, sortie) :
	"""Écrit le résultat du prétraitement du corpus sur la sortie.
	@param tws Les tweets nettoyés. 
		Type : [[str]]
	@param sortie Le fichier de sortie. 
		Type : fichier
	"""
	for tw in tws :
		sortie.write(' '.join(tw) + '\n')


def __main__(args) :
	"""
	Programme principal. 
	"""
	__ecrire_tws(lecture(args['corpus'], hashtags=args['hashtags'], radic=args['radicalisation']), args['output'])


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 lecture.py")
	
	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help="Le corpus de tweets au même format que Tache2_Train.csv.")

	parser.add_argument('-o', '--output', type=argparse.FileType('w'), 
		help="Le fichier dans lequel écrire les tweets sous forme de liste de mots séparés par un espace. Sortie standard par défaut.", 
		default=sys.stdout)

	parser.add_argument('--hashtags',
		help="Garder les hashtags intacts",
		action="store_true")

	parser.add_argument('--radicalisation',
		help="Radicalise les mots",
		action="store_true")

	__main__(vars(parser.parse_args()))