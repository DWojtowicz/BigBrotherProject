#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Implémentation de la technique LSA.

http://mccormickml.com/2016/03/25/lsa-for-text-classification-tutorial/


Inspiré du code de Chris McCormick
https://github.coam/chrisjmccormick/LSA_Classification

@author Damien Wojtowicz
"""

import argparse
from sklearn.feature_extraction import text as sktext
from sklearn.decomposition import truncated_svd as sksvd
import lecture


def charger_corpus(corpus, est_propore):
	"""Charge le corpus.

	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper

	@param est_propre Le corpus a déjà subi un prétraitement.
		Type : bool

	@return Le corpus au comme étant une liste de documents.
		Type : [str]
	"""
	if est_propore :
		return [line for line in corpus]
	else :
		return [" ".join(doc) for doc in lecture.lecture(corpus)]



def lsa(corpus, nb_sujets, top_n, min_df=2, max_df=0.5) :
	"""Implémentation de l'algorithme LSA.

	https://www.quora.com/What-is-the-best-latent-semantic-analysis-in-Python

	@param corpus Le corpus de documents sous forme de liste de tweets.
		Type : [str]

	@param nb_sujets Le nombre de sujets à trouver dans le corpus.
		Type : int, > 0

	@param top_n Le nombre de mots à utiliser pour définir un sujet.
		Type : int,  > 0

	@param min_df Seuil minimal de DF d'un mot.
		Type : int
		Défaut : 2

	@param max_df Le ratio maximal du nombre de documents dans lequel un mot peut apparaître.
		Type : float
		Défaut : 0.5

	@return Le liste des `nb_sujets` sous forme de liste des `top_n` mots les 
			plus caractéristiques du sujet, présentés sous la forme d'un couple mot/score.
		Type : [[(str, float)]]

	"""
	# Calcul des TFxIDF
	vectorizer = sktext.TfidfVectorizer(min_df=min_df, max_df=max_df)
	trans = vectorizer.fit_transform(corpus)
	
	# Recherche des sujets
	lsa = sksvd.TruncatedSVD(n_components=nb_sujets, n_iter=1000)
	lsa.fit(trans)
	
	# Mise en forme du retour de la fonction
	terms = vectorizer.get_feature_names()
	topics = []
	for i, comp in enumerate(lsa.components_) :
		topics.append(sorted(zip(terms, comp), key=lambda x: x[1], reverse=True)[:top_n])

	return topics



def __main__(args) :

	corp = charger_corpus(args['corpus'], args['clean'])
	topics = lsa(corp, args['num_topics'], args['top_n'])

	# Affichage des résultats
	for topic in topics :
		print("\t".join([x[0] for x in topic]))


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 lsa.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help='Le corpus de tweets en sortie de pretraitement.py')

	parser.add_argument('--num_topics', type=int, 
		help='Le nombre de thèmes à trouver dans le corpus.', 
		default=25)

	parser.add_argument('--top_n', type=int, 
		help='Le nombre de mots définissant un thème.', 
		default=5)

	parser.add_argument('--clean', 
		help="Précise si le corpus en entrée est le résultat des prétraitements de lecture.py.", 
		action="store_true")

	__main__(vars(parser.parse_args()))