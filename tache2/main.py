#!/usr/bin/env python3
# -*- coding: utf8 -*-

import argparse
import lecture
import lsa
import lda
import topic_modelling


def __main__(args):
	corp = [" ".join(doc) for doc in lecture.lecture(args['corpus'])]
	topics = topic_modelling.model_topics(corp, lsa.lsa, 23)

	# print(perplexity(topics))

	for topic in topics :
		print("\t".join([x[0] for x in topic]))


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 main.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help='Le corpus de tweets en sortie de pretraitement.py')

	__main__(vars(parser.parse_args()))