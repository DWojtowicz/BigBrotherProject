#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Utilitaires permettant de tokeniser et lemmatiser des mots.

Les scripts ne prennent pas en compte les erreurs de language ni les erreurs de typographie difficilement
détectables, telles que les oublis d'espaces entre les mots.
"""

from nltk.stem.snowball import FrenchStemmer
from nltk.corpus import stopwords


__CONTRACT_D2 = ["l\'", "d\'", "n\'", "c\'", "s\'", "j\'", "y\'",  \
					"l’", "d’", "n’", "c’", "s’", "ç’", "j’", "y’", \
					"l`", "d`", "n`", "c`", "s`", "ç`", "j`", "y`"]
"""Liste des contractions en début de mot de taille 2.
"""

__CONTRACT_D3 = ["qu\'", "qu’", "qu`"]
"""Liste des contractions en début de mot de taille 3.
"""

__CONTRACT_F5 = ["-t-il", "-t-on", "-nous"]
"""Liste des contractions en fin de mot de taille 5.
"""

__CONTRACT_F6 = ["-t-ils"]
"""Liste des contractions en fin de mot de taille 6.
"""

__CONTRACT_F7 = ["-t-elle"]
"""Liste des contractions en fin de mot de taille 7.
"""

__CONTRACT_F8 = ["-t-elles"]
"""Liste des contractions en fin de mot de taille 8.
"""

__PONCTUATION = ['#', '@', '(', '[', '{', ')', ']', '}', \
					"«", "»", ".", "?", "!", ";", ",", \
					"\"", "\'", "“", "`", "_", "-", ":", \
					"…", "‼", "️", "©"]
"""Liste des caractères de ponctuation.
"""

__MOTS_VIDES_SUPPLEMENTAIRES = ['à', 'où', 'la', 'le', 'il', 'est', \
					'ici', 'ce', 'car', 'vi', 'ça', 'les', 'plus', \
					'ils', 'via', 'là', \
					'pk', 'qd', 'ki', 'pr', 'av', 'ac', 'pdt', 'ke', \
					'ki', 'kwa', "koi", \
					"être", "jamais", "va", "si", "non", "trop", "ni", \
					"ca", "très", "aujourd", "hui", "sans", "avoir", \
					"jusqu", "au", "vais", "donc", "oui", "dis", "aussi", \
					"fait", "voulait", "tous", "tout", "dit", "aimais"]
"""Des mots vides n'étant pas forcément présents dans NLTK
"""

__stemmer = FrenchStemmer()
"""Le lemmatiseur
"""


def mot_acceptable(mot) :
	"""Détermine si un mot est acceptable ou non pour le corpus.

	Un mot acceptable n'est ni un URL, ni un caractère spécial,
	ni un mot vide.
	@param mot Le mot à tester 
		Type : str
	@return boolean
	"""
	mot_test = str(mot)


	test = mot_test.isdigit()
	if test :
		return False

	test = mot_test.startswith("http")
	if test :
		return False

	test = ("&" in mot_test and ";" in mot_test)
	if test :
		return False

	test = (mot_test[0] in ("«", "»", ".", "?", "!"))
	if test :
		return False

	test = (mot_test in stopwords.words('french'))
	if test :
		return False

	test = (mot_test in __MOTS_VIDES_SUPPLEMENTAIRES)
	if test :
		return False

	test = (mot_test.startswith("là-")) 

	return (not test)


def __nettoyage_contractions(mot) :
	"""Supprime les contractions type l', d', etc. en début de mot.
	@param mot Le mot à traiter. 
		Type : str
	@return Le mot traité. 
		Type : str
	"""
	if any(mot.startswith(e) for e in __CONTRACT_D2) :
		return __nettoyage_contractions(mot[2:])

	elif any(mot.startswith(e) for e in __CONTRACT_D3) :
		return __nettoyage_contractions(mot[3:])

	elif any(mot.endswith(e) for e in __CONTRACT_F5) :
		return __nettoyage_contractions(mot[:-5])

	elif any(mot.endswith(e) for e in __CONTRACT_F6) :
		return __nettoyage_contractions(mot[:-6])

	elif any(mot.endswith(e) for e in __CONTRACT_F7) :
		return __nettoyage_contractions(mot[:-7])

	elif any(mot.endswith(e) for e in __CONTRACT_F8) :
		return __nettoyage_contractions(mot[:-8])

	else :
		return mot


def traitement_mot(mot, hashtags=False, radic=True) :
	"""Nettoie un mot des caractères indésirables et effectue une lemmatisation.
	@param mot Le mot à traiter. 
		Type : str
	@param hashtags Indique si les hashtags doivent rester intacts ou non. 
		Type : bool
		Défaut : False
	@param hashtags Indique si les mots doivent être radicalisés ou non. 
		Type : bool
		Défaut : True
	@return Le mot traité. 
		Type str
	"""
	mot_min = mot.lower()

	if not (hashtags and mot_min.startswith('#')) :
		mot_min = __nettoyage_contractions(mot_min)

		# Élimination des parenthèses, crochets ou autres trucs
		while any(mot_min.startswith(e) for e in __PONCTUATION):
			mot_min = mot_min[1:]

	# Élimination des parenthèses fermantes
	while any(mot_min.endswith(e) for e in __PONCTUATION):
		mot_min = mot_min[:-1]

	if not (hashtags and mot_min.startswith('#')) :
		mot_min = __nettoyage_contractions(mot_min)

		return radic and __stemmer.stem(mot_min) or mot_min

	else :
		return mot_min
