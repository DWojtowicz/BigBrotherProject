#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Implémente la recherche du meilleur topic modelling possible pour un corpus donné."""
import lsa

def continuer_dicho(mem_topics, nb_topic) :
	"""Détermine si le numéro de topic "optimal" a été trouvé

	@param mem_topics La liste des topics déjà calculés.
		Type : [[nb_topics -> int, score -> float, topics -> [[(str, float)]] ]]

	@param nb_topic Le nombre de sujets à trouver dans le corpus.
		Type : int

	@return bool La recherche du "bon" numéro de topic continue ou non.
	"""

	# Doit retourner True pour ces conditions (vpor prochain_nombre_topics)
		# SI mem_topics = []
		# SI len(mem_topics) = 1
		# SI len(mem_topics) = 2
	tmp1 = []
	tmp2 = []
	tmp3 = [] 
	if len(mem_topics) < 4:
		return True
	else:
		length = len(mem_topics)
		tmp1.append(mem_topics[length])
		tmp2.append(mem_topics[length - 1])
		tmp3.append(mem_topics[length -2])
		if tmp1[]

	pass


def prochain_nombre_topics(mem_topics, nb_topic=None, borne_inf=10, borne_sup=40) :
	"""Retourne le prochain nombre de topics à évaluer.

	@param mem_topics La liste des topics déjà calculés.
		Type : [[nb_topics -> int, score -> float, topics -> [[(str, float)]] ]]

	@param nb_topic Le nombre de sujets à trouver dans le corpus.
		Type : int
		Default : None

	@param borne_inf La borne inférieure de l'espace de recherche.
		Type : int
		Défaut : 10

	@param borne_inf La borne supérieure de l'espace de recherche.
		Type : int
		Défaut : 40

	@return Le nombre de topics suivant.
		Type : int
	"""
	mem_topics.append([nb_mileu,lsa(corpus,borne_inf)])
	mem_topics.append([nb_mileu,lsa(corpus,borne_sup)])
	nb_mileu = (borne_inf + borne_sup) // 2 
	mem_topics.append([nb_mileu,lsa(corpus,nb_mileu)])


	# SI mem_topics = [], on retourne, borne_inf
	# SI len(mem_topics) = 1, on retourne borne_sup
	# SI len(mem_topics) = 2 ET nb_topics != None, on retourne nb_topic
	pass


def model_topics(corpus, algo, nb_topics=None):
	"""Chercher le "meilleur" topic modelling possible dans le corpus.

	@param corpus Le corpus des tweets
		Type : [[str]]

	@param algo 
		Type : Fonction, type : algo(corpus -> [[str]], nb_topics -> int)

	@param nb_topics Le nombre de topics à tester.
		Type : int
		Default : None

	@return Le topic modelling le plus "optimal"
		Type : [[(str, float)]]
	"""


	# Mémoire des topics déjà calculés. Type : [[nb_topics -> int, score -> float, topics -> [[(str, float)]] ]]
	mem_topics = []

	# def lsa(corpus, nb_sujets, top_n, min_df=2, max_df=0.5)
	# def lda(corpus, nb_sujets, top_n, min_df=1, max_df=0.5)
	return algo(corpus, nb_topics, 3)