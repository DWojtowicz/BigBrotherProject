Here is a list of the names of python scripts used during the process of extracting communities with the input of a list of tweets.

### Data extraction

`get_users.py`: extracting the userIds of the people having emitted the tweets and the hashtag used for the tweet	


`get_FollowersAndFriends.py`: collecting all the edges of the graph: going through all users and save all connections to other users having emitted a 										   tweet in the list of tweets. The output is a file containing all edges in a directed graph

`community.py`: use igraph to calculate the cluster of communities using a method that aims at maximizing the modularity score. As output, 										 a file containing on each line a userId and it's assigned cluster


### Validating result

`get_statuses.py` : extracting the userIds of the people having emitted the tweets, the text of the tweet and the hashtag used for the tweet	

`get_textes.py`: extracting the tweets emitted within a cluster and put them in seperate file. The file format is adapted to fit into the 									   LDA of tache 2: in 3 columns, the two first are non important (here, we just put an "a"), the third is the tweet. 

`makeLastCSV.py`: fusion and formatting of different files to be able to read and visualize the result in Gephi