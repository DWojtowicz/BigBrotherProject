#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
script destined at using igraph to calculate the cluster of communities. The method aims at maximizing the modularity score og edges. As output, 										 
a file containing on each line a userId and it's assigned cluster
Also, comparing two communities calculated with two different algorithms
"""


from igraph import *
import numpy as np
import csv



##### community structure based on maximizing the modularity score ##############
#################################################################################


def maximizing_modularity(graph, outputfile):
	"""using igraph to calculate community structure based on edge-betweeness and write the result to a .csv-file
	@param graph: a file containing the edges of a directed graph
		Type : .edge-file (format csv-file) 
	@param outputfile: the name of the file to print the output
	"""
	twitter_igraph = Graph.Read_Ncol(graph, directed=True)
	#calculate dendogram
	dendogram = twitter_igraph.community_edge_betweenness()
	# # convert into a flat clustering
	clusters = dendogram.as_clustering()
	# # get membership vector
	membership = clusters.membership
	# # write to file
	writer = csv.writer(open(outputfile, 'w'))
	for name, membership in zip(twitter_igraph.vs['name'], membership):
 		writer.writerow([name, membership])



def compare_two_partitions (A,B):
	"""compare two partitions: calculating their nmi
	@param A a community
		Type :  VertexClustering object
	@param B a community
		Type :  VertexClustering object
	@return their nmi
		Type : float
	"""

	res = compare_communities(A,B, method = "nmi", remove_none = False)
	return res

###############################################################################################################


############ infomap algorithm community ###########
infomap = twitter_igraph.community_infomap()

############ label propagation algorithm community#######
label_prop = twitter_igraph.community_label_propagation()


print ("comparison between infomap and label propagation, the result is: ")
print(compare_two_partitions(infomap, label_prop))


maximizing_modularity('directed_graph.csv', 'result_modularity.csv')