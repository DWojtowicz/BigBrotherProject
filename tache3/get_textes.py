#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
for each cluster, extracting the tweets emitted, filetering thim with a regular expression and formatting them 
to be able to pass into he LDA program developed in tache 2
"""


import tweepy
import re



# adapting tweets  into the right format for the LDA algorithm
def get_tweets(file, userList,outputfile):
	tweet_file = open(file, 'r')
	output = open(outputfile,'w')

	tweets = tweet_file.readlines()
	p = re.compile("\d+\\t.+")

	for line in tweets:
		lnew = line.rstrip('\n')
		m = p.match(lnew)
		if m :
			tokens = lnew.split('\t')
			user = tokens[0]

			if (int(user) in userList):
				tweet = tokens[1]
				output.write("a"+"\t"+ "a" + "\t" + tweet + "\n")
				print (user + ',' + tweet)
		else:
			pass


	output.close()
	tweet_file.close()






def get_tweets_by_cluster(userfile, clusternumber,outputfile):
		"""extracting tweets emitted by one single cluster and adapting the output file to fitting into the LDA
	@param userfile: a file containing all user IDs and their cluster ID
		Type :  text or csv file
	@param clusternumber the cluster ID number
		Type :  integer
	@param outputfile: the file to print the result
		Type : a file name
	@param tweetfile: the file containing all the tweets and the ID of the user having emitted the tweet
		Type : a file name
	"""
	user_file = open(userfile, 'r')
	userCluster = []
	output = open(outputfile,'w')
	

	users = user_file.readlines()

	for line in users:
		lnew = line.rstrip('\n')

		tokens = lnew.split(',')
		user = tokens[0]
		cluster = int(tokens[3])
		if (cluster == clusternumber):
			userCluster.append(int(user)) 

	
	print(userCluster)
	tweet_file = open("data/statuses.txt", 'r')

	tweets = tweet_file.readlines()
	p = re.compile("\d+\\t.+")

	compteur = 0
	for line in tweets:
		lnew = line.rstrip('\n')
		m = p.match(lnew)
		if m :
			tokens = lnew.split('\t')
			user = int(tokens[0])
			text = tokens[1]
			# print (user)
			# print (text)
			if (user in userCluster):
				print (text)
				compteur += 1
				output.write("a"+"\t"+ "a" + "\t" + text + "\n")
	

	


	user_file.close()
	output.close()
	tweet_file.close()
	print (compteur)
	print (len(userCluster))







# users = get_users("test.csv")
# print (users)

# get_tweets("statuses.txt", users, "textes.txt")
#get_tweets_by_cluster("result_modularity.csv", 1, "cluster_1.csv")
#get_tweets_by_cluster("result_modularity.csv", 80, "cluster_80.csv")
get_tweets_by_cluster("data/modularity_classes_gephi.csv", 1, "data/gephi_class1.csv")
get_tweets_by_cluster("data/modularity_classes_gephi.csv", 17, "data/gephi_class17.csv")
get_tweets_by_cluster("data/modularity_classes_gephi.csv", 4, "data/gephi_class4.csv")
get_tweets_by_cluster("data/modularity_classes_gephi.csv", 16, "data/gephi_class16.csv")





# users = get_users("test.csv")
# print (users)

# get_tweets("statuses.txt", users, "textes.txt")
# get_tweets_by_cluster("data/result_modularity.csv", 1, "data/cluster_1.csv","data/statuses.txt")
# get_tweets_by_cluster("data/result_modularity.csv", 80, "data/cluster_80.csv","data/statuses.txt")




