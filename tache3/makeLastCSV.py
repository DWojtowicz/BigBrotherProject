#!/usr/bin/env python
# -*- coding: utf-8 -*-

# script for building a .csv file in format readable in Gephi:
# userID, clusterID, hashtag


def makeClusters(clusterFile):
	""" extract clusters from a file containing all userIds and their attributed cluster
	@param userfile the file containing users and their cluster
	    Type : csv-file

	@return cluster a list of lists, the index of the list being the clusterId and the list containing the users attributed
	    Type : [[]]
	"""
	
	nbOfClusters = 0
	file = open(clusterFile, "r")

	userData = file.readlines()

	# get number of clusters in file
	for line in userData:

		try:
			lnew = line.rstrip('\n')
			tokens = lnew.split(",")
			clusterID = int(tokens[1])
			if clusterID > nbOfClusters:
				nbOfClusters = clusterID
		except Exception as e: print (e);

	# list of lists of size number of clusters to contain userIds. The index of the list is the clusterID
	clusters = [[] for x in range(0,nbOfClusters+1)]

	# add users to their attributed cluster
	for line in userData:

		try:
			lnew = line.rstrip('\n')
			tokens = lnew.split(",")
			userId = int(tokens[0])
			clusterID = int(tokens[1])
			clusters[clusterID].append(userId)
		except Exception as e: print (e);


	return clusters



def get_hashtags_from_users(clusterFile,hashtagFile,outputfile):
	""" 
	outputs a file being a fusion of userID, the hashtag used in their tweet and their clusterID
	@param clusterFile a file containing a line for each user: userID,clusterID
	    Type : csv-file

	@param hashtagFile a file containing a line for each user: userID,hashtag used in their tweet
	    Type : csv-file

	@param outputfile, each line being of the form: userID, clusterID, hashtag
	    Type : csv-file

	"""
	myClusters = makeClusters(clusterFile)


	hashtagF = open(hashtagFile, "r")
	output = open(outputfile,"w")
	
	hashData = hashtagF.readlines()

	for line in hashData:

		try:
			lnew = line.rstrip('\n')
			tokens = lnew.split(",")
			user = int(tokens[0])
			tag = str(tokens[1])
			# print (user)
			# print (tag)
			for i in range(len(myClusters)):
				if (user in myClusters[i]):
					print (user, i, tag)
					output.write(str(user) + ',' + str(i) + ',' + str(tag) +'\n')
			

		except Exception as e: print (e);

	hashtagF.close()
	output.close()



 ###############################################


get_hashtags_from_users('data/result_modularity.csv','data/userIdAndHashtag.csv','data/myLastCSV.csv')
