#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
implementation de la classification avec naive bayes, features bag of words
"""
import numpy as np
import argparse
import pandas as pd

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import GlobalAveragePooling1D
from keras.datasets import imdb
from keras.preprocessing.text import Tokenizer
from keras import optimizers
from keras.utils import np_utils
from keras.models import Model

import loadFile
import matrix_evaluation

max_features = 20000
maxlen = 500
batch_size = 32
embedding_dims = 50
epochs = 60

#charger corpus
def __main__(args):
	#Load training data
	train = pd.read_csv(args['train'], header=0, delimiter="\t", quoting=3)
	test = pd.read_csv(args['test'], header=0, delimiter="\t", quoting=3)
	
	# Keras's tokenizer turns each word into a word index, that will be
	# used to look up the word's embedding. We first construct its
	# vocabulary by fitting it on the training data; max_features defines
	# how many words will be taken into account
	tokenizer = Tokenizer(num_words=max_features, lower=True, split=" ")
	tokenizer.fit_on_texts(train["tweet"])

	# construct mappings from words to indices and vice versa
	w2i = tokenizer.word_index
	i2w = {i:w for w,i in w2i.items()}

	# now we turn each word into its word index; the sequence of indices
	# (x_train) will be the input to our network, while y_train is the
	# output we train the network on
	x_train = tokenizer.texts_to_sequences(train["tweet"])
	x_train = np.asarray(x_train)
	#y_train = train["classes"]
	y_train = np_utils.to_categorical(np.asarray(train["classes"]))

	# same for the test data
	x_test = tokenizer.texts_to_sequences(test["tweet"])
	x_test = np.asarray(x_test)

	# Because we use batches, each sequence needs to be the same length;
	# we therefore "pad" the sequences, which means we create fixed-length
	# vector and fill up the positions that are not used with zeros
	print('Pad sequences')
	x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
	x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
	print('x_train shape:', x_train.shape)
	print ('x_test shape:', x_test.shape)

	print ('Build model...')

	# First we build our model. We define a sequential model to which we
	# can add network layers
	model = Sequential()

	# We start off with an embedding layer which maps
	# our indices into embeddings of size embedding_dims
	model.add(Embedding(max_features, embedding_dims, input_length=maxlen))

	# We add a GlobalAveragePooling1D, which will average the embeddings
	# of all words in the document
	model.add(GlobalAveragePooling1D())
	model.add(Dense(16, activation='relu'))

	# We project onto a single unit output layer, and squash it with a
	# sigmoid. This is similar to what we did in the former exercise: as
	# we only have two classes, we can just predict a value between 0
	# (negative) and positive (1)
	model.add(Dense(4, activation='sigmoid'))

	# Once our model is constructed, we can compile it
	model.compile(loss='categorical_crossentropy', optimizer='adam',
	              metrics=['accuracy'])

	# Now we train our model on the training set and validate on the test
	# set
	model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs)


	pred_array = model.predict(x_test)
	pred = []
	for i in range(len(pred_array)):
		pred.append(np.argmax(pred_array[i]))

	loadFile.write_predict_cbow(pred)


	"""
	pred = []
	for i in range(len(pred_array)):
		pred.append(np.argmax(pred_array[i]))

	labels = test['classes']
	#print('le vrai classes',labels)
	print('le prediction',pred)

	prec_obj, prec_neg,prec_mix,prec_pos = matrix_evaluation.matrix_precision(pred,labels)
	avg_precision = (prec_obj+prec_neg+prec_mix+prec_pos)/4.0 
	print("precision objective",prec_obj)
	print("precision negative",prec_neg)
	print("precision mixed",prec_mix)
	print("precision positive",prec_pos)

	rap_obj, rap_neg,rap_mix,rap_pos = matrix_evaluation.matrix_rappel(pred,labels)
	avg_rappel = (rap_obj + rap_neg + rap_mix + rap_pos)/4.0
	print("rappel objective",rap_obj)
	print("rappel negative",rap_neg)
	print("rappel mixed",rap_mix)
	print("rappel positive",rap_pos)

	f_measure = 2 * (avg_precision * avg_rappel) / (avg_rappel + avg_precision)
	print("average rappel",avg_rappel)
	print("average precision", avg_precision)
	print("f_measure",f_measure)
	"""

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 classification_cbow.py")

	parser.add_argument('train', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')
	parser.add_argument('test', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')

	__main__(vars(parser.parse_args()))