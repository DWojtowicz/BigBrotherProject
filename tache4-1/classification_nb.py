#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
implementation de la classification avec naive bayes, features bag of words
"""

import sys
import argparse

import loadFile
import matrix_evaluation

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GroupKFold



def kFlodModel(classifier,train_data_features,classes,tws_id):
	"""
	@param la classifeur
	@param le donnees de traitement 
	@param les classes associe a chaque tweets
	@param le id de chaque tweets
	@return la list de prediction, la list de classes associe a les donnes de test, le score de prediction
		Type [[],[],float]
	"""
	group_kfold = GroupKFold(n_splits=10)
	score_kfold = []
	pred = []
	for train_index, test_index in group_kfold.split(train_data_features,classes,tws_id):
		X_train,X_test = train_data_features[train_index],train_data_features[test_index]
		y_train,y_test = classes[train_index],classes[test_index]

		classifier.fit(X_train,y_train)	
		score_kfold.append(classifier.score(X_test,y_test))
		pred.append(classifier.predict(X_test))

	max_index = score_kfold.index(max(score_kfold))

	meilleur_pred = pred[max_index]

	print("k-fold scores",score_kfold)
	print("le bon class",y_test)
	print("predict class",meilleur_pred)
	return meilleur_pred,y_test,score_kfold[max_index]



def __main__(args):

	#tws_id,classes,tweets = charger_corpus_pre(args['corpus'])
	tws_id,classes,tweets= loadFile.charger_corpus_no_pre(args['corpusTrain'])
	tws_id1,tweets1= loadFile.charger_corpusTest(args['corpusTest'])
	
	corpusTrain = [" ".join(doc) for doc in tweets]
	corpusTest = [" ".join(doc) for doc in tweets1]

	tout_mots = set([mt for tw in corpusTrain for mt in tw.split()])
	tout_mots |= set([mt for tw in corpusTest for mt in tw.split()])

	vectorizer_tfidf = TfidfVectorizer(vocabulary=tout_mots) 

	train_data_features = vectorizer_tfidf.fit_transform(corpusTrain)
	test_data_features = vectorizer_tfidf.fit_transform(corpusTest)

	classifier = MultinomialNB()
	classifier.fit(train_data_features,classes)	
	pred = classifier.predict(test_data_features)
	loadFile.write_predict_nb(pred)

	"""	
	pred,labels,score= kFlodModel(classifier,train_data_features,classes,tws_id)
	prec_obj, prec_neg,prec_mix,prec_pos = matrix_evaluation.matrix_precision(pred,labels)
	avg_precision = (prec_obj+prec_neg+prec_mix+prec_pos)/4.0 
	print("precision objective",prec_obj)
	print("precision negative",prec_neg)
	print("precision mixed",prec_mix)
	print("precision positive",prec_pos)

	rap_obj, rap_neg,rap_mix,rap_pos = matrix_evaluation.matrix_rappel(pred,labels)
	avg_rappel = (rap_obj + rap_neg + rap_mix + rap_pos)/4.0
	print("rappel objective",rap_obj)
	print("rappel negative",rap_neg)
	print("rappel mixed",rap_mix)
	print("rappel positive",rap_pos)

	f_measure = 2 * (avg_precision * avg_rappel) / (avg_rappel + avg_precision)
	print("average rappel",avg_rappel)
	print("average precision", avg_precision)
	print("f_measure",f_measure)"""

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 classification_nb.py")

	parser.add_argument('corpusTrain', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')

	parser.add_argument('corpusTest', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')

	__main__(vars(parser.parse_args()))

