#!/usr/bin/env python

import pandas as pd
import numpy as np
import re
import argparse
import sklearn

from sklearn.feature_extraction.text import  TfidfVectorizer
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.utils import np_utils
from keras import optimizers

import loadFile
import matrix_evaluation


def __main__(args):

	tws_id,classes,tweets= loadFile.charger_corpus_no_pre(args['corpusTrain'])
	tws_id1,tweets1= loadFile.charger_corpusTest(args['corpusTest'])
	
	corpusTrain = [" ".join(doc) for doc in tweets]
	corpusTest = [" ".join(doc) for doc in tweets1]

	tout_mots = set([mt for tw in corpusTrain for mt in tw.split()])
	tout_mots |= set([mt for tw in corpusTest for mt in tw.split()])

	vectorizer= TfidfVectorizer(vocabulary=tout_mots) 


	# fit_transform() performs two operations; first, it fits the model
	# and learns the vocabulary; second, it transforms our training data
	# into feature vectors. The input to fit_transform should be a list of
	# strings.
	train_data_features = vectorizer.fit_transform(corpusTrain)

	# output from vectorizer is a sparse array; our classifier needs a
	# dense array
	x_train = train_data_features.toarray()

	# construct a matrix of two columns (one for positive class, one for
	# negative class) where the correct class is indicated with 1 and the
	# incorrect one with 0
	print(classes)
	y_train = np_utils.to_categorical(np.asarray(classes))

	# same process for test set
	test_data_features = vectorizer.transform(corpusTest)
	x_test = test_data_features.toarray()

	print('Build model')

	# construct a neural network model with
	# 1) an input layer of MAX_FEATURES neurons
	# 2) a hidden layer with 4 neurons, with relu activation applied
	# 3) an output layer of 2 neurons, with softmax applied to ensure
	#    a probability distribution
	model = Sequential()
	model.add(Dense(16, input_shape=(len(tout_mots),)))
	model.add(Activation('relu'))
	model.add(Dense(16))
	model.add(Activation('relu'))
	model.add(Dense(16))
	model.add(Activation('relu'))
	model.add(Dense(16))
	model.add(Activation('relu'))
	model.add(Dense(4))
	model.add(Activation('softmax'))

	# the model uses cross-entropy as a loss function, finds the best
	# parameters using stochastic gradient descent, and prints accuracy
	# metrics
	sgd = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.8, nesterov=True)
	model.compile(loss='categorical_crossentropy', optimizer='sgd',
	              metrics=['acc'])

	# now we train the model by feeding it a batch of 8 training samples
	# at a time. When we get to the end of the training set, we repeat the
	# process, and we do this 10 times (epochs). We validate our model on
	# the test set (remember though: when we test different model
	# parameters we should make use of a validation set separate from the
	# test set)
	model.fit(x_train, y_train,epochs=40, batch_size=8)

	pred_array = model.predict(x_test)
	pred = []
	for i in range(len(pred_array)):
		pred.append(np.argmax(pred_array[i]))

	loadFile.write_predict_nn(pred)

	"""
	prec_obj, prec_neg,prec_mix,prec_pos = matrix_evaluation.matrix_precision(pred,labels)
	avg_precision = (prec_obj+prec_neg+prec_mix+prec_pos)/4.0 
	print("precision objective",prec_obj)
	print("precision negative",prec_neg)
	print("precision mixed",prec_mix)
	print("precision positive",prec_pos)

	rap_obj, rap_neg,rap_mix,rap_pos = matrix_evaluation.matrix_rappel(pred,labels)
	avg_rappel = (rap_obj + rap_neg + rap_mix + rap_pos)/4.0
	print("rappel objective",rap_obj)
	print("rappel negative",rap_neg)
	print("rappel mixed",rap_mix)
	print("rappel positive",rap_pos)

	f_measure = 2 * (avg_precision * avg_rappel) / (avg_rappel + avg_precision)
	print("average rappel",avg_rappel)
	print("average precision", avg_precision)
	print("f_measure",f_measure)"""


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 classification_nn.py")

	parser.add_argument('corpusTrain', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')

	parser.add_argument('corpusTest', type=argparse.FileType('r'), 
		help='Le corpus de tweets.')

	__main__(vars(parser.parse_args()))