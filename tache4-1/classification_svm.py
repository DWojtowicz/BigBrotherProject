#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
implementation de la classification avec un classifier svm features bag of words
kernel: linear
prend en compte la probabilité d'une classe
"""

import sys
import argparse
import numpy as np

import loadFile
import matrix_evaluation
import traitement_tweets

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.model_selection import GroupKFold
from sklearn.svm import SVC



def charger_corpus_no_pre(corpus):
	"""
	charger le corpus
	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper
	@return les id de tweets and labels de tweets et le sack de mot pour chaque tweete
		tws_id : list
		classes: arrary
		tweets : list
	"""
	donnees = loadFile.lecture_no_pre(corpus)
	tws_id  = [ int(tws[0]) for tws in donnees ]
	classes = [ tws[1] for tws in donnees ]
	tweets = [ tws[2:] for tws in donnees ]

	nb_obj = 0
	nb_neg = 0
	nb_mix = 0
	nb_pos = 0
	for i in range(len(classes)):
		if (classes[i] == 'objective'):
			classes[i] = 0
			nb_obj = nb_obj + 1
		elif (classes[i] == 'negative'):
			classes[i] = 1
			nb_neg = nb_neg + 1
		elif (classes[i] == 'mixed'):
			classes[i] = 2
			nb_mix = nb_mix + 1
		else:
			classes[i] = 3
			nb_pos = nb_pos + 1
	classes = np.asarray(classes)

	print("type de tweets no pretait",type(tweets))
 
	return (tws_id,classes,tweets)


def charger_corpus_pre(corpus):
	"""
	charger le corpus
	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper
	@return les id de tweets and labels de tweets et le sack de mot pour chaque tweete
		tws_id : list
		classes: arrary
		tweets : list
	"""
	donnees = loadFile.lecture_pre(corpus)
	tws_id  = [ int(tws[0]) for tws in donnees ]
	classes = [ tws[1] for tws in donnees ]
	tweets = [ tws[2:] for tws in donnees ]
	nb_obj = 0
	nb_neg = 0
	nb_mix = 0
	nb_pos = 0
	for i in range(len(classes)):
		if (classes[i] == 'objective'):
			classes[i] = 0
			nb_obj = nb_obj + 1
		elif (classes[i] == 'negative'):
			classes[i] = 1
			nb_neg = nb_neg + 1
		elif (classes[i] == 'mixed'):
			classes[i] = 2
			nb_mix = nb_mix + 1
		else:
			classes[i] = 3
			nb_pos = nb_pos + 1
	classes = np.asarray(classes)
	print("type de tweets pretait",type(tweets))
 
 
	return (tws_id,classes,tweets)


def classifier_TFIDF_model(X_train,labels,y_train,y_test):
	"""tf_idf model"""
	vectorizer_tfidf = TfidfTransformer()
	train_data_features = vectorizer_tfidf.fit_transform(X_train)
	classifier = MultinomialNB().fit(X_train,labels)

	test_data_features = vectorizer_tfidf.fit_transform(y_train)
	score = classifier.score(test_data_features,y_test)

	return (classifier,score)


def kFlodModel(classifier,train_data_features,classes,tws_id):
	"""
	@param la classifeur
	@param le donnees de traitement 
	@param les classes associe a chaque tweets
	@param le id de chaque tweets
	@return la list de prediction, la list de classes associe a les donnes de test, le score de prediction
		Type [[],[],float]
	"""
	group_kfold = GroupKFold(n_splits=10)
	score_kfold = []
	pred = []
	for train_index, test_index in group_kfold.split(train_data_features,classes,tws_id):
		X_train,X_test = train_data_features[train_index],train_data_features[test_index]
		y_train,y_test = classes[train_index],classes[test_index]

		classifier.fit(X_train,y_train)	
		score_kfold.append(classifier.score(X_test,y_test))
		pred.append(classifier.predict(X_test))

	max_index = score_kfold.index(max(score_kfold))

	meilleur_pred = pred[max_index]
	print("score",score_kfold)
	print("le bon class",y_test)
	print("predict class",meilleur_pred)
	return meilleur_pred,y_test,score_kfold[max_index]


def __main__(args):


	"""
	si corpus sans pretraitement, commenter ligne 148 et decommenter ligne 149
	si corpus avec pretraitement, commenter ligner 149 et decommenter ligne 149
	"""
	tws_id,classes,tweets = charger_corpus_pre(args['corpus'])
	#tws_id,classes,tweets = charger_corpus_no_pre(args['corpus'])
	
	corpus = [" ".join(doc) for doc in tweets]

	tout_mots = set([mt for tw in corpus for mt in tw.split()])

	vectorizer_tfidf = TfidfVectorizer(vocabulary=tout_mots) 

	train_data_features = vectorizer_tfidf.fit_transform(corpus)

	classifier = SVC(kernel='linear', C=1, probability=True)
	pred,labels,score= kFlodModel(classifier,train_data_features,classes,tws_id)
	prec_obj, prec_neg,prec_mix,prec_pos = matrix_evaluation.matrix_precision(pred,labels)
	avg_precision = (prec_obj+prec_neg+prec_mix+prec_pos)/4.0 
	print("precision objective",prec_obj)
	print("precision negative",prec_neg)
	print("precision mixed",prec_mix)
	print("precision positive",prec_pos)

	rap_obj, rap_neg,rap_mix,rap_pos = matrix_evaluation.matrix_rappel(pred,labels)
	avg_rappel = (rap_obj + rap_neg + rap_mix + rap_pos)/4.0
	print("rappel objective",rap_obj)
	print("rappel negative",rap_neg)
	print("rappel mixed",rap_mix)
	print("rappel positive",rap_pos)

	f_measure = 2 * (avg_precision * avg_rappel) / (avg_rappel + avg_precision)
	print("average rappel",avg_rappel)
	print("average precision", avg_precision)
	print("f_measure",f_measure)

	print("==============================================")

	corpus_eval = []
	for line in args['corpus_test'] :
		if not (len(line) == 1 or line.startswith("/⁠/")) :
			corpus_eval.append(traitement_tweets.traiter_tweet(line.split("\t")[1]))

	corpus_eval = [" ".join(tw) for tw in corpus_eval]

	tfidf_mat = vectorizer_tfidf.fit_transform(corpus_eval)
	features = tfidf_mat.toarray()

	pred = classifier.predict(features)
	print(pred)
	for i in range(len(pred)) :
		if pred[i] == 0 :
			print("%d\tobjective" % (i+1))
		elif pred[i] == 1 :
			print("%d\tnegative" % (i+1))
		elif pred[i] == 2 :
			print("%d\tmixed" % (i+1))
		else :
			print("%d\tpositive" % (i+1))

	#tws_id,classes,tweets = charger_corpus_no_pre(args['corpus'])
	



if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 classification_svm.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help='Le corpus de tweets annote.')

	parser.add_argument('corpus_test', type=argparse.FileType('r'), 
		help='Le corpus de tweets non annote.')

	__main__(vars(parser.parse_args()))

