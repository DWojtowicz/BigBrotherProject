#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Prétraitements pour le corpus de tweets.

Il s'agit d'un script pouvant être exécuté à part (voir documentation dans le terminal : python3 lecture.py -h).
La fonction `lecture` qu'il propose peut être embarquée dans du code, de sorte à récupérer une liste de tweets, 
étant eux-mêmes des listes de chaines de caractères représentant les mots du corpus nettoyés et lemmatisés.
"""

import sys
import argparse
import numpy as np
import pandas as pd

import nltk

import traitement_tweets

def lectureTest(fic):
	"""Lit le contenu du fichier de tweets pour le test.
	@param fic Le fichier CSV de tweets. Du format 'ID\tTAG\tTWEET'. 
		Type : TextIOWrapper
	@return La liste des tweets. 
		Type : [str]
	"""
	tweets = []
	# Lecture des lignes contenant des tweets
	for ligne in fic:
		if not (ligne.startswith("//") or ligne.startswith("/⁠/⁠")) and len(ligne) > 1:

			tmp = [ligne.split('\t')[0]]
			for word in ligne.split('\t')[1].split():
				word.strip().lower()
				if len(word) > 1 :
					tmp.append(word)
			if tmp != [] :
				tweets.append(tmp)
	return tweets

def lecture_no_pre(fic):
	"""Lit le contenu du fichier de tweets et effectue la tokenisation.
	@param fic Le fichier CSV de tweets. Du format 'ID\tTAG\tTWEET'. 
		Type : TextIOWrapper
	@return La liste des tweets. 
		Type : [str]
	"""
	tweets = []
	# Lecture des lignes contenant des tweets
	for ligne in fic:
		if not (ligne.startswith("//") or ligne.startswith("/⁠/⁠")) and len(ligne) > 1:
			tmp = [ligne.split('\t')[0],ligne.split('\t')[2][:-1]]
			for word in ligne.split('\t')[1].split():
				word.strip().lower()
				if len(word) > 1 :
					tmp.append(word)
			if tmp != [] :
				tweets.append(tmp)
	return tweets


def lecture_pre(fic) :
	"""Lit le contenu du fichier de tweets et effectue la tokenisation.
	@param fic Le fichier CSV de tweets. Du format 'ID\tTAG\tTWEET'. 
		Type : TextIOWrapper
	@param hashtags Indique si les hashtags doivent rester intacts ou non. 
		Type : bool
		Défaut : False
	@param radic Indique si les mots doivent être radicalisés ou non. 
		Type : bool
		Défaut : False
	@return La liste des tweets. 
		Type : []
	"""
	tweets = []
	# Lecture des lignes contenant des tweets
	for ligne in fic:
		if not (ligne.startswith("//") or ligne.startswith("/⁠/⁠")) and len(ligne) > 1:
			
			tmp = [ligne.split('\t')[0],ligne.split('\t')[2][:-1]," ".join(traitement_tweets.traiter_tweet(ligne.split('\t')[1]))]
			"""
			tmp.append(traitement_tweets.traiter_tweet(ligne.split('\t')[1]))
			tmp = " ".join(tmp)						
			for word in ligne.split('\t')[1].split():
				
				# Filtrage des mots vides et des "impuretés"
				if mot_acceptable(word.strip().lower()) :
					word = traitement_mot(word.strip(), hashtags, radic)

					# On vérifie à nouveau si le mot est OK
					# car des mots vides ressortent parfois du traitement
			b		if len(word) > 1 and mot_acceptable(word) :
						tmp.append(word)
			"""

			if tmp != [] :
				tweets.append(tmp)

	return tweets

def charger_corpusTest(corpus):
	"""
	charger le corpus
	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper
	@return les id de tweets and labels de tweets et le sack de mot pour chaque tweete
		tws_id : list
		classes: arrary
		tweets : list
	"""
	donnees = lectureTest(corpus)
	tws_id  = [ int(tws[0]) for tws in donnees[1:] ]
	tweets = [ tws[1:] for tws in donnees[1:] ]

	return (tws_id,tweets)

def charger_corpus_no_pre(corpus):
	"""
	charger le corpus
	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper
	@return les id de tweets and labels de tweets et le sack de mot pour chaque tweete
		tws_id : list
		classes: arrary
		tweets : list
	"""
	donnees = lecture_no_pre(corpus)
	tws_id  = [ int(tws[0]) for tws in donnees]
	classes = [ tws[1] for tws in donnees ]
	tweets = [ tws[2:] for tws in donnees ]

	nb_obj = 0
	nb_neg = 0
	nb_mix = 0
	nb_pos = 0
	for i in range(len(classes)):
		if (classes[i] == 'objective'):
			classes[i] = 0
			nb_obj = nb_obj + 1
		elif (classes[i] == 'negative'):
			classes[i] = 1
			nb_neg = nb_neg + 1
		elif (classes[i] == 'mixed'):
			classes[i] = 2
			nb_mix = nb_mix + 1
		else:
			classes[i] = 3
			nb_pos = nb_pos + 1
	classes = np.asarray(classes)
	print('le resultat no pretraitement')
 
	return (tws_id,classes,tweets)


def charger_corpus_pre(corpus):
	"""
	charger le corpus
	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper
	@return les id de tweets and labels de tweets et le sack de mot pour chaque tweete
		tws_id : list
		classes: arrary
		tweets : list
	"""
	donnees = lecture_pre(corpus)
	tws_id  = [ int(tws[0]) for tws in donnees ]
	classes = [ tws[1] for tws in donnees]
	tweets = [ tws[2:] for tws in donnees]
	nb_obj = 0
	nb_neg = 0
	nb_mix = 0
	nb_pos = 0
	for i in range(len(classes)):
		if (classes[i] == 'objective'):
			classes[i] = 0
			nb_obj = nb_obj + 1
		elif (classes[i] == 'negative'):
			classes[i] = 1
			nb_neg = nb_neg + 1
		elif (classes[i] == 'mixed'):
			classes[i] = 2
			nb_mix = nb_mix + 1
		else:
			classes[i] = 3
			nb_pos = nb_pos + 1
	classes = np.asarray(classes)
	print('le resultat avec pretraitement')
 
	return (tws_id,classes,tweets)

def write_predict_nb(predict):
	prediction = open("prediction_nb.txt","w+")
	lclass = []
	for i in range(len(predict)):
		if predict[i] == 0:
			classe = str('objective')
		elif predict[i] == 1:
			classe = str('negative')
		elif predict[i] == 2:
			classe = str('mixed')
		else:
			classe = str('positive')
		prediction.write("%i\t%s\n"%(i+1,classe))


def write_predict_cbow(predict):
	prediction = open("prediction_cbow.txt","w+")
	lclass = []
	for i in range(len(predict)):
		if predict[i] == 0:
			classe = str('objective')
		elif predict[i] == 1:
			classe = str('negative')
		elif predict[i] == 2:
			classe = str('mixed')
		else:
			classe = str('positive')
		prediction.write("%i\t%s\n"%(i+1,classe))

def write_predict_nn(predict):
	prediction = open("prediction_nn.txt","w+")
	lclass = []
	for i in range(len(predict)):
		if predict[i] == 0:
			classe = str('objective')
		elif predict[i] == 1:
			classe = str('negative')
		elif predict[i] == 2:
			classe = str('mixed')
		else:
			classe = str('positive')
		prediction.write("%i\t%s\n"%(i+1,classe))


def __ecrire_tws(tws, sortie) :
	"""Écrit le résultat du prétraitement du corpus sur la sortie.
	@param tws Les tweets nettoyés. 
		Type : [[str]]
	@param sortie Le fichier de sortie. 
		Type : fichier
	"""
	#print(tws)
	for tw in tws :
		sortie.write(' '.join(tw) + '\n')


def __main__(args) :
	"""
	Programme principal. 
	"""
	lecture_no_pre(args['corpus'])
	#tws=lecture(args['corpus'], hashtags=args['hashtags'], radic=args['radicalisation']), args['output']
	lecture_pre(args['corpus'])

	
