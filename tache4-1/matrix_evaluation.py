#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
implementation de la classification avec naive bayes, features bag of words
"""


def matrix_rappel(predict,classes):
	"""
	@param la list de prediction sur les donnees de test
	@param la list de vrai classes associe a les donnees de test
	@return precison pour le 4 classes
		Type List
	"""
	pred_obj = 0
	pred_neg = 0
	pred_mix = 0
	pred_pos = 0
	classes = list(classes)
	nb_obj = classes.count(0)
	nb_neg = classes.count(1)
	nb_mix = classes.count(2)
	nb_pos = classes.count(3)
	for i in range(len(classes)):
		if(predict[i]==classes[i]):
			if(classes[i] == 0):
				pred_obj += 1
			elif(classes[i] == 1):
				pred_neg += 1
			elif(classes[i] == 2):
				pred_mix +=1
			else:
				pred_pos +=1
		else:
			pass
	rappel_obj = pred_obj / nb_obj * 1.0
	rappel_neg = pred_neg / nb_neg * 1.0
	rappel_mix = pred_mix / nb_mix * 1.0
	rappel_pos = pred_pos / nb_pos * 1.0

	return rappel_obj,rappel_neg,rappel_mix,rappel_pos


def matrix_precision(predict,classes):
	"""
	@param la list de prediction sur les donnees de test
	@param la list de vrai classes associe a les donnees de test
	@return precison pour le 4 classes
		Type List
	"""

	pred_obj = 0
	pred_neg = 0
	pred_mix = 0
	pred_pos = 0
	predict = list(predict)
	nb_obj = predict.count(0)
	nb_neg = predict.count(1)
	nb_mix = predict.count(2)
	nb_pos = predict.count(3)
	for i in range(len(classes)):
		if(predict[i]==classes[i]):
			if(classes[i] == 0):
				pred_obj += 1
			elif(classes[i] == 1):
				pred_neg += 1
			elif(classes[i] == 2):
				pred_mix +=1
			else:
				pred_pos +=1
		else:
			pass
	precision_obj = pred_obj / (nb_obj * 1.0 + 1)
	precision_neg = pred_neg / (nb_neg * 1.0 + 1)
	precision_mix = pred_mix / (nb_mix * 1.0 + 1)
	precision_pos = pred_pos / (nb_pos * 1.0 + 1)

	return precision_obj,precision_neg,precision_mix,precision_pos


