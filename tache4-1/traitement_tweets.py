#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Fonctions de traitement des tweets pour la détection d'opinion.
"""

import sys
import os
from nltk.stem.snowball import FrenchStemmer



"""
__FIC_EMOT_PATH = os.path.dirname(os.path.abspath(__file__)) + "/emoticons.txt"
"""
"""Chemin vers le fichier des emojis
"""

__stemmer = FrenchStemmer()
"""Le lemmatiseur
"""

__FIC_EMOT_POS_PATH = os.path.dirname(os.path.abspath(__file__)) + "/emoticons_pos.txt"
"""Chemin vers le fichier des emojis
"""

__FIC_EMOT_NEG_PATH = os.path.dirname(os.path.abspath(__file__)) + "/emoticons_neg.txt"
"""Chemin vers le fichier des emojis
"""

__PONCT_INSIGNIFIANTE = ["," , ";", ":"]
""" Liste des symboles de ponctuation sans signification.
"""

__PONCT_UTILE = ["«", "»", "?", "!", "\"", "“"]

"""
essaie de definir mots vide
"""

"""
__REMPL = [("l\'", ""), ("l’", ""), ("l`", ""), 
			 ("d\'", ""), ("d’", ""), ("d`", ""), 
			 ("n\'", " ne "), ("n’", " ne "), ("n`", " ne "), 
			 ("c\'", ""), ("c’", ""), ("c`", ""), 
			 ("s\'", ""), ("s’", ""), ("s`", ""), 
			 ("ç\'", ""), ("ç’", ""), ("ç`", ""), 
			 ("j\'", ""), ("j’", ""), ("j`", ""), 
			 ("y\'", ""), ("y’", ""), ("y`", ""),
			 ("t\'", ""), ("t’", ""), ("t`", ""), 
			 ("z\'", ""), ("z’", ""), ("z`", ""), 
			 ("(", ""), (")", ""), ("[", ""), ("]", ""),
			 ("qu\'", ""), ("qu’", ""), ("qu`", ""), 
			 # L'écriture inclusive....... En partie seulement..... https://www.scribd.com/document/351478550/Manuel-d-ecriture-inclusive
			 (".e.", ""), ("-e-", ""), ("(e)", ""), ("·e·", ""), \
			 (".e", ""), ("-e", ""), ("·e", ""), \
			 ("la·le", ""), ("la.le", ""), ("la(le)", ""), ("la-le", ""), \
			 ("le·la", ""), ("le.la", ""), ("le(la)", ""), ("le-la", ""), \
			 ("du·de", ""), ("du.de", ""), ("du(de)", ""), ("du-de", ""), \
			 ("la", ""), \
		  	 ("‼", " ! "), ("«", " \" "), ("»", " \" "), ("“", " \" "), ("?", " ? ")]

"""
"""Liste de la ponctuation porteuse de sens
"""

__REMPL = [("l\'", "le "), ("l’", "le "), ("l`", "le "), 
			 ("d\'", "de "), ("d’", "de "), ("d`", "de "), 
			 ("n\'", "ne "), ("n’", "ne "), ("n`", "ne "), 
			 ("c\'", "ce "), ("c’", "ce "), ("c`", "ce "), 
			 ("s\'", "se "), ("s’", "se "), ("s`", "se "), 
			 ("ç\'", "ça "), ("ç’", "ça "), ("ç`", "ça "), 
			 ("j\'", "je "), ("j’", "je "), ("j`", "je "), 
			 ("y\'", "il y "), ("y’", "il y "), ("y`", "il y "),
			 ("t\'", "tu "), ("t’", "tu "), ("t`", "tu "), 
			 ("z\'", "vous "), ("z’", "vous "), ("z`", "vous "), 
			 ("qu\'", "que "), ("qu’", "que "), ("qu`", "que "), 
			 # L'écriture inclusive....... En partie seulement..... https://www.scribd.com/document/351478550/Manuel-d-ecriture-inclusive
			 (".e.", ""), ("-e-", ""), ("(e)", ""), ("·e·", ""), \
			 (".e", ""), ("-e", ""), ("·e", ""), \
			 ("la·le", "le"), ("la.le", "le"), ("la(le)", "le"), ("la-le", "le"), \
			 ("le·la", "le"), ("le.la", "le"), ("le(la)", "le"), ("le-la", "le"), \
			 ("du·de", "du"), ("du.de", "du"), ("du(de)", "du"), ("du-de", "du"), \
			 ("la", "le"), \
		  	 ("‼", " ! ! "), ("«", " \" "), ("»", " \" "), ("“", " \" "), ("?", " ? ")]

"""Liste des remplacements.
"""


__REPL_FIN = [("(", ""), (")", ""), ("[", ""), ("]", "")]

"""Remplacements à faire à la fin du traitement d'un mot
__REPL_FIN = [("(", " ( "), (")", " ) "), ("[", " [ "), ("]", " ] ")]
"""

with open(__FIC_EMOT_POS_PATH, "r") as emo :
	__EMOTICONS_POS = [line[:-1] for line in emo]
	"""Liste des emoticônes.
	"""

with open(__FIC_EMOT_NEG_PATH, "r") as emo :
	__EMOTICONS_NEG = [line[:-1] for line in emo]
	"""Liste des emoticônes.
	"""


def __traiter_mot(mot, lst_mots) :
	""" Fontion traitant les mots.

	@param mot Le mot à traiter
		Type : str

	@param lst_mots La liste retournant les mots traités.
		Type : [str]

	@return None
	"""
	# Si le mot est une URL, on l'ignore
	if mot.startswith("http") or mot.startswith("bit.ly") :
		mot = "🌏"
		lst_mots.append(mot)
		return

	# Si le mot est un emoticone, on le garde
	if any(mot in emo for emo in __EMOTICONS_POS) :
		lst_mots.append("bien")
		return

		# Si le mot est un emoticone, on le garde
	if any(mot in emo for emo in __EMOTICONS_NEG) :
		lst_mots.append("pas")
		return

	# Suppression de la ponctuation insignifiante
	if any(mot in pnct for pnct in __PONCT_INSIGNIFIANTE):
		return

	mot = mot.strip(''.join(__PONCT_INSIGNIFIANTE))
	if len(mot) == 0 :
		return

	# Suppression des points seuls
	if mot == "." :
		return
	elif len(mot) > 2 and mot[-1] == "." and mot[-2] != "." :
		mot = mot[:-1]

	# Cas des pointillés :
	if mot.count("..") > 0 :
		for smot in mot.split(".") :
			if smot != "" :
				__traiter_mot(smot, lst_mots)
		lst_mots.append("…")
		return

	elif mot[-1] == "…" :
		__traiter_mot(mot[:-1], lst_mots)
		lst_mots.append("…")
		return

	# Cas des hashtags (à ne pas confondre avec les emojis)
	if "#" in mot and "&#" not in mot :
		for sm in mot.split("#") :
			if len(sm) > 0 :
				__traiter_mot(sm, lst_mots)
		return

	# Cas des noms d'utilisateur.trice.s (à ne pas confondre avec les emojis)
	if "@" in mot and "&@" not in mot :
		for sm in mot.split("@") :
			if len(sm) > 0 :
				__traiter_mot(sm, lst_mots)
		return

	# Cas du reste de la ponctuation
	"""
	for rpl in __REPL_FIN :
		mot = mot.replace(rpl[0], rpl[1])
	"""

	sms = mot.split()
	if len(sms) > 1 :
		for sm in sms :
			__traiter_mot(sm, lst_mots)
		return

	if len(mot) > 0 :
		res = __stemmer.stem(mot) 
		lst_mots.append(res)



def traiter_tweet(tw) :
	"""Fonction traitant un tweet.
	Les contractions (du type "j'") sont remplacées par leurs pronoms, les caractères
	de ponctuation sont décollés des mots et séparés les uns des autres.

	@param tw Un tweet
		Type : str

	@return Le tweet nettoyé
		Type : [str]
	"""

	lst_mots = []	# La liste des mots et de la ponctuation à retourner.

	# Séparation de la ponctuation avec du sens
	for char in __PONCT_UTILE :
		tw = tw.replace(char, " " + char + " ")

	# Remlpacement de certains symboles
	for repl in __REMPL :
		tw = tw.replace(repl[0], repl[1])

	# Parcours des mots
	for tok in tw.split() :
		__traiter_mot(tok.lower(), lst_mots)

	return lst_mots