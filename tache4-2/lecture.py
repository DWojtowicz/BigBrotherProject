#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Prétraitements pour le corpus de tweets.

Il s'agit d'un script pouvant être exécuté à part (voir documentation dans le terminal : python3 lecture.py -h).
La fonction `lecture` qu'il propose peut être embarquée dans du code, de sorte à récupérer une liste de tweets, 
étant eux-mêmes des listes de chaines de caractères représentant les mots du corpus nettoyés et lemmatisés.
"""

import sys
import argparse
import traitement_tweets


def lecture(fic, hashtags=False, etiquetes=False) :
	"""Lit le contenu du fichier de tweets et effectue la tokenisation.
	@param fic Le fichier CSV de tweets. Du format 'ID\tTAG\tTWEET'. 
		Type : TextIOWrapper
	@param hashtags Indique si les hashtags doivent rester intacts ou non. 
		Type : bool
		Défaut : False
	@param etiquetes Indique si les lignes du fichier ont des étiquettes
		Type : bool
		Défaut : False
	@return La liste des tweets. 
		Type : [str]
	@return La liste des étiquettes de tweets s'ils sont étiquetés. 
		Type : [str]
	"""
	tweets = []
	tmp = None
	etiquettes = []

	# Lecture des lignes contenant des tweets
	for ligne in fic:
		if not (ligne.startswith("//") or ligne.startswith("/⁠/⁠")) and len(ligne) > 1:
			
			tweets.append(" ".join(traitement_tweets.traiter_tweet(ligne.split('\t')[1])))
			if etiquetes :
				etiquettes.append(ligne.split('\t')[2][:-1])

	if etiquettes :
		return tweets, etiquettes
	else :
		return tweets


def __ecrire_tws(tws, polarite, sortie) :
	"""Écrit le résultat du prétraitement du corpus sur la sortie.
	@param tws Les tweets nettoyés.
		Type : [[str]]
	@param tws Les polarites.
		Type : [str]
	@param sortie Le fichier de sortie. 
		Type : fichier
	"""
	pol_bis = []
	for p in polarite :
		if p == "figurative" : 
			pol_bis.append(0)
		elif p == "nonfigurative" : 
			pol_bis.append(1)
		else :
			raise ValueError("La polarité du tweet est inconnue.\nElle devrait être :\n\tfigurative\n\tnonfigurative")

	for i in range(len(tws)) :
		sortie.write(str(i) + "\t" + ' '.join(tws[i]) + "\t" + str(pol_bis[i]) + '\n')


def __main__(args) :
	"""
	Programme principal. 
	"""
	tws, pol = lecture(args['corpus'], hashtags=args['hashtags'], radic=args['radicalisation'])
	__ecrire_tws(tws, pol, args['output'])


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 lecture.py")
	
	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help="Le corpus de tweets au même format que Tache2_Train.csv.")

	parser.add_argument('-o', '--output', type=argparse.FileType('w'), 
		help="Le fichier dans lequel écrire les tweets sous forme de liste de mots séparés par un espace. Sortie standard par défaut.", 
		default=sys.stdout)

	parser.add_argument('--hashtags',
		help="Garder les hashtags intacts",
		action="store_true")

	parser.add_argument('--radicalisation',
		help="Radicalise les mots",
		action="store_true")

	__main__(vars(parser.parse_args()))