#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
implementation de la classification avec naive bayes, features bag of words
"""


def matrix_rappel(predict,classes):
	"""
	@param la list de prediction sur les donnees de test
	@param la list de vrai classes associe a les donnees de test
	@return precison pour les 2 classes
		Type List
	"""
	pred_fig = 0
	pred_nfig = 0
	classes = list(classes)
	nb_obj = classes.count(0)
	nb_neg = classes.count(1)
	for i in range(len(classes)):
		if predict[i] == classes[i] :
			if classes[i] == 0 :
				pred_fig += 1
			else:
				pred_nfig += 1
		else:
			pass
	rappel_obj = pred_fig / nb_obj * 1.0
	rappel_neg = pred_nfig / nb_neg * 1.0

	return rappel_obj, rappel_neg


def matrix_precision(predict,classes):
	"""
	@param la list de prediction sur les donnees de test
	@param la list de vrai classes associe a les donnees de test
	@return precison pour les 2 classes
		Type List
	"""

	pred_fig = 0
	pred_nfig = 0
	predict = list(predict)
	nb_obj = predict.count(0)
	nb_neg = predict.count(1)
	for i in range(len(classes)):
		if(predict[i]==classes[i]):
			if(classes[i] == 0):
				pred_fig += 1
			else :
				pred_nfig += 1
		else:
			pass
	precision_fig = pred_fig / (nb_obj * 1.0 + 1)
	precision_nfig = pred_nfig / (nb_neg * 1.0 + 1)

	return precision_fig, precision_nfig

def calcul_fmesure(precision_fig, precision_nfig, rappel_fig, rappel_nfig) :
	""" Calcule la F-mesure d'une classification.

	F = 2*(précision*rappel)/(précision+rappel)
	
	@param precision_fig La précision de la classe figurative
		Type : float
	@param precision_nfig La précision de la classe nonfigurative
		Type : float
	@param rappel_fig Le rappel de la classe figurative
		Type : float
	@param rappel_nfig Le rappel de la classe nonfigurative
		Type : float
	@return La f-mesure
		Type : float
	"""
	prec = (precision_fig + precision_nfig) / 2
	rapp = (rappel_fig + rappel_nfig) / 2
	
	return 2 * (prec * rapp) / (prec + rapp)
