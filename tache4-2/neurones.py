#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Détection d'ironie par apprentissage profond.

Entraînement d'un réseau de neurones
"""

# TAGS : figurative
#        nonfigurative

# Tuto word2vec 
# http://nbviewer.jupyter.org/github/danielfrg/word2vec/blob/master/examples/word2vec.ipynb

import argparse
import random
import os
import sys
import pickle

# Désactivation de la sortie d'erreur à cause des imports...
f = open(os.devnull, 'w')
stderr_backup = sys.stderr
sys.stderr = f

import pandas as pd
import numpy as np
import re
import sklearn

from sklearn.feature_extraction.text import CountVectorizer

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.utils import np_utils
from keras.callbacks import Callback
from keras.callbacks import EarlyStopping
from keras.models import model_from_json

sys.stderr = stderr_backup

import lecture
import matrix_evaluation

MAX_FEATURES = 2500

def entrainement_modele(in_train_tws, in_train_pols, in_eval, in_eval_pols, sans_proba=False) :
	"""Fonction entrainant un nouveau modèle.
	@param in_train_tws Les tweets de l'ensemble d'apprentissage
		Type : [str]
	@param in_train_pols La polarité pour l'ensemble d'apprentissage
		Type : [str]
	@param in_eval Les tweets de l'ensemble d'évaluation
		Type : [str]
	@param in_eval_pols La polarité pour l'ensemble d'évaluation
		Type : [str]
	@param sans_proba Indique si les résultats ne doivent pas être modulés par la probabilité a priori.
		Type : bool
		Default : True
	@return model Le réseau de neurones généré
		Type : keras.models.Sequential
	@return vectorizer Le vectorizer utilisé pour le batch
		Type : sklearn.feature_extraction.text.CountVectorizer
	@return rappel_pos Le rappel obtenu sur la classe "figurative"
		Type : float
	@return rappel_neg Le rappel obtenu sur la classe "nonfigurative"
		Type : float
	@return precision_pos La précision obtenue sur la classe "figurative"
		Type : float
	@return precision_neg La précision obtenue sur la classe "nonfigurative"
		Type : float
	"""

	# Changement des polarités
	pol_bis = []
	for p in in_train_pols :
		if p == "figurative" : 
			pol_bis.append(0)
		elif p == "nonfigurative" : 
			pol_bis.append(1)
		else :
			print(p)
			raise ValueError("La polarité du tweet est inconnue.\nElle devrait être :\n\tfigurative\n\tnonfigurative")
	in_train_pols = pol_bis

	pol_bis = []
	for p in in_eval_pols :
		if p == "figurative" : 
			pol_bis.append(0)
		elif p == "nonfigurative" : 
			pol_bis.append(1)
		else :
			print(p)
			raise ValueError("La polarité du tweet est inconnue.\nElle devrait être :\n\tfigurative\n\tnonfigurative")
	in_eval_pols = pol_bis

	# Découpage en ensembles d'entrainement et de test
	train_tws = []
	train_pol = []
	fit_tws = []
	fit_pol = []

	for i in range(len(in_train_tws)) :
		if random.randint(0, 10) != 1 :
			train_tws.append(in_train_tws[i])
			train_pol.append(in_train_pols[i])
		else :
			fit_tws.append(in_train_tws[i])
			fit_pol.append(in_train_pols[i])

	#
	# Vectorisation des entrées
	#
	vec_tmp = CountVectorizer(
	    analyzer = "word",
	    min_df=3
	)

	mat_tmp = vec_tmp.fit_transform(in_train_tws)
	liste_mots = vec_tmp.get_feature_names()


	vectorizer = CountVectorizer(
	    analyzer = "word",
	    vocabulary = liste_mots,
	    min_df=3
	)

	# Vectorisation de la vérité terrain
	train_data_features = vectorizer.fit_transform(train_tws)
	x_train = train_data_features.toarray()
	y_train = np_utils.to_categorical(np.asarray(train_pol))

	# Vectorisation de l'ensemble d'apprentissage
	test_data_features = vectorizer.transform(fit_tws)
	x_test = test_data_features.toarray()
	y_test = np_utils.to_categorical(np.asarray(fit_pol))

	# Vectorisation de l'ensemble à évaluer
	evaluation_data_features = vectorizer.fit_transform(in_eval)
	evaluation_data_features = evaluation_data_features.toarray()

	#
	# Déclaration et entraînement du réseau de neurones
	#

	model = Sequential()

	model.add(Dense(4, input_shape=(len(vectorizer.get_feature_names()),)))
	model.add(Activation('relu'))
	model.add(Dense(2))
	model.add(Activation('softmax'))
	
	model.compile(loss='categorical_crossentropy', optimizer='sgd',
	              metrics=['acc'])
	
	model.fit(x_train, y_train, validation_data=(x_test, y_test),
	          epochs=12, batch_size=5, verbose=0)

	#
	# Catégorisation des tweets à évaluer
	#

	# Prédiction
	res = model.predict(evaluation_data_features)

	# Modulation de la prédiction avec la probabilité a priori de trouver la classe
	prob_fig = (train_pol.count(0) + fit_pol.count(0)) / len(in_train_pols)
	
	if not sans_proba :
		res = res*np.array([prob_fig, 1-prob_fig])
	
	lst_res = [r.argmax() for r in res]

	rappel_pos, rappel_neg = matrix_evaluation.matrix_rappel(lst_res, in_eval_pols)
	precision_pos, precision_neg = matrix_evaluation.matrix_precision(lst_res, in_eval_pols)
	
	return model, vectorizer, prob_fig, rappel_pos, rappel_neg, precision_pos, precision_neg


def __main__(args) :
	"""
	Programme principal. 
	"""

	print("# No\tRappel +\tRappel -\tPrécision +\tPrécicion -\tF-mesure\tMeilleur")

	#
	# Chargement des corpus
	#
	in_train_tws, in_train_pols = lecture.lecture(args["corpus_train"], etiquetes=True)
	
	#
	# Recherche du meilleur réseau
	#
	res = [None, None, 0.0, 0.0, 0.0, 0.0, 0.0] # Même format que le retour de entrainement_modele
	res_f_mesure = 0.0
	res_tmp = []
	res_tmp_f_mesure = 0.0
	cpt = 0 # Compteur pour les tests
	meilleur = 0
	for it in range(args["nb_iterations_entrainement"]) :
		train_tws = []
		train_pols = []
		eval_tws = []
		eval_pols = []

		# Découpage en ensembles d'entraînement et de test
		for i in range(len(in_train_tws)) :
			if random.randint(0, 10) != 1 :
				train_tws.append(in_train_tws[i])
				train_pols.append(in_train_pols[i])
			else :
				eval_tws.append(in_train_tws[i])
				eval_pols.append(in_train_pols[i])

		# Itérations multiples sur le même réseau 
		for i in range(args["nb_sous_iterations_entrainement"]) :
			res_tmp = entrainement_modele(train_tws, train_pols, eval_tws, eval_pols, args["pas_proba_a_priori"])
			res_tmp_f_mesure = matrix_evaluation.calcul_fmesure(res_tmp[3], res_tmp[4], res_tmp[5], res_tmp[6])
			cpt += 1

			# print("--------------------")
			# print("Réseau %d" % (cpt))
			# print("Rappel : \tfig : %.3f\tnfig : %.3f" % (res_tmp[3], res_tmp[4]))
			# print("Précision : \tfig : %.3f\tnfig : %.3f" % (res_tmp[5], res_tmp[6]))
			# print("F-mesure : \t%.3f" % (res_tmp_f_mesure))


			# On garde le meilleur résultat obtenu
			if res_f_mesure <= res_tmp_f_mesure :
				res = res_tmp
				res_f_mesure = res_tmp_f_mesure
				meilleur = cpt
				# print("\tMeilleur réseau courant !")
				print("%d\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t1" % (cpt, res_tmp[3], res_tmp[4], res_tmp[5], res_tmp[6], res_tmp_f_mesure))
			else :
				print("%d\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t0" % (cpt, res_tmp[3], res_tmp[4], res_tmp[5], res_tmp[6], res_tmp_f_mesure))

	print("# ==================================================")
	print("# Réseau final : n°%d" % (meilleur))
	print("# Rappel : \tfig : %.3f\tnfig : %.3f" % (res[3], res[4]))
	print("# Précision : \tfig : %.3f\tnfig : %.3f" % (res[5], res[6]))
	print("# F-mesure : \t%.3f" % (res_f_mesure))

	#
	# Sauvrgarde du meilleur réseau
	#

	print("")
	print("# Sauvegarde du réseau de neurones...")

	model_json = res[0].to_json()
	with open("tache4-2/ressources/model.json", "w") as json_file:
	    json_file.write(model_json)

	res[0].save_weights("tache4-2/ressources/model.h5")

	with open("tache4-2/ressources/vectorizer.pk", "wb") as f :
		pickle.dump(res[1], f, pickle.HIGHEST_PROTOCOL)

	print("# Sauvegarde terminée !")


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 neurones.py")

	parser.add_argument('--pas_proba_a_priori', 
		help="Pondération des résultats avec les probas a priori.", 
		action="store_true")

	parser.add_argument('--nb_iterations_entrainement', type=int, 
		help='Le d\'itérations à effectuer pour l`entrainement.', 
		default=2)

	parser.add_argument('--nb_sous_iterations_entrainement', type=int, 
		help='Le d\'itérations à effectuer pour l`entrainement.', 
		default=4)
	
	parser.add_argument('corpus_train', type=argparse.FileType('r'), 
		help="Le corpus de tweets d'entraînement au même format que Tache2_Train.csv.")

	__main__(vars(parser.parse_args()))