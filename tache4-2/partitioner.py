import random
import sys

f_train = open("../BBP_res/tache42/train.csv", "w+")
f_test = open("../BBP_res/tache42/test.csv", "w+")
f_eval = open("../BBP_res/tache42/eval.csv", "w+")
f_in = open("../BBP_res/Tache4-2_Train.csv", "r")

f_train.write("\ttweet\tpolarite\n")
f_test.write("\ttweet\tpolarite\n")

for line in f_in :
	if random.randint(0, 10) != 1 :
		f_train.write(line)
	elif random.randint(0, 10) != 2 :
		f_eval.write(line)
	else : 
		f_test.write(line)

f_train.close()
f_test.close()
f_eval.close()
f_in.close()