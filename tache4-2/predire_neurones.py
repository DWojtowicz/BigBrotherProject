#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""Détection d'ironie par apprentissage profond.

Utilisation d'un réseau de neurones préentraîné
"""

import argparse
import os
import sys
import pickle

# Désactivation de la sortie d'erreur à cause des imports...
f = open(os.devnull, 'w')
stderr_backup = sys.stderr
sys.stderr = f

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.utils import np_utils
from keras.callbacks import Callback
from keras.callbacks import EarlyStopping
from keras.models import model_from_json

sys.stderr = stderr_backup

import lecture

def __main__(args):

	# Chargement du vectoriseur
	f_vec = open('tache4-2/ressources/vectorizer.pk', 'rb')
	vectorizer = pickle.load(f_vec)
	f_vec.close()

	# Chargement du réseau de neurones
	json = open('tache4-2/ressources/model.json', 'r')
	model_json = json.read()
	json.close()
	model = model_from_json(model_json)
	model.load_weights("tache4-2/ressources/model.h5")
	model.compile(loss='categorical_crossentropy', optimizer='sgd',
	              metrics=['acc'])
	 
	# Chargement du corpus
	in_tws = lecture.lecture(args["corpus"], etiquetes=False)
	corpus_vec = vectorizer.fit_transform(in_tws)
	features = corpus_vec.toarray()

	# Prédictions
	pred = model.predict(features)
	for i in range(len(pred)) :
		if pred[i].argmax() == 0 :
			print("%d\tfigurative" % (i+1))
		else :
			print("%d\tnonfigurative" % (i+1))


if __name__ == '__main__':

	parser = argparse.ArgumentParser(prog="python3 predire_neurones.py")

	parser.add_argument('corpus', type=argparse.FileType('r'), 
		help="Le corpus sur lequel appliquer la détection d'ironie.")

	__main__(vars(parser.parse_args()))