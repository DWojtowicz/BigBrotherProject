﻿from sklearn import ensemble
import pretraitement
import lecture
import argparse
import random
import graphviz 
import os
import subprocess
import numpy as np
import matrix_evaluation as me
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder

MAX_FEATURES = 2500

def charger_corpus(corpus, est_propore):
	"""Charge le corpus.

	@param corpus Le fichier contenant le corpus.
		Type : TextIOWrapper

	@param est_propre Le corpus a déjà subi un prétraitement.
		Type : bool

	@return Le corpus au comme étant une liste de documents.
		Type : [str]
	"""
	if est_propore :
		return [line for line in corpus]
	else :
		return [" ".join(doc) for doc in lecture.lecture(corpus)]

def visualize_tree(tree):
    """Create tree png using graphviz.

    Args
    ----
    tree -- scikit-learn DecsisionTree.
    feature_names -- list of feature names.
    """
    with open("dt.dot", 'w') as f:
        export_graphviz(tree, out_file=f)

    command = ["dot", "-Tpng", "dt.dot", "-o", "dt.png"]
    try:
        subprocess.check_call(command)
    except:
        exit("Could not run dot, ie graphviz, to "
             "produce visualization")


def __main__(args) :
	"""
	Programme principal. 
	"""

	#
	# Chargement des corpus et séparation des ensembles de vérité terrain et d'apprentissage
	#
	
	# Chargement des corpus
	in_train_tws, in_train_pols = lecture.lecture(args["corpus_train"], etiquetes=True)
	in_eval = lecture.lecture(args["corpus_eval"], etiquetes=False)
	labels = [0,1]
	# Changement des polarités
	pol_bis = []
	for p in in_train_pols :
		if p == "figurative" : 
			pol_bis.append(0)
		elif p == "nonfigurative" : 
			pol_bis.append(1)
		else :
			print(p)
			raise ValueError("La polarité du tweet est inconnue.\nElle devrait être :\n\tfigurative\n\tnonfigurative")
	in_train_pols = pol_bis
	features = in_train_tws

	# Découpage en ensembles d'entrainement et de test
	train_tws = []
	train_pol = []
	fit_tws = []
	fit_pol = []

	# for i in range(len(in_train_tws)) :
		# if random.randint(0, 10) != 1 :
			# train_tws.append(in_train_tws[i])
			# train_pol.append(in_train_pols[i])
		# else :
			# fit_tws.append(in_train_tws[i])
			# fit_pol.append(in_train_pols[i])
			
	for i in range(len(in_train_tws)) :
		train_tws.append(in_train_tws[i])
		train_pol.append(in_train_pols[i])
	for i in range(len(in_eval)):
		fit_tws.append(in_eval[i])
			
	#
	# Vectorisation des entrées
	#
	vectorizer = CountVectorizer(
	    analyzer = "word",
	    max_features = MAX_FEATURES
	)

	le = LabelEncoder()
	# Vectorisation de la vérité terrain
	train_data_features = vectorizer.fit_transform(train_tws)
	x_train = train_data_features.toarray()
	y_train = le.fit_transform(np.asarray(train_pol))

	# Vectorisation de l'ensemble d'apprentissage
	test_data_features = vectorizer.transform(fit_tws)
	x_test = test_data_features.toarray()
	#y_test = le.fit_transform(np.asarray(fit_pol))

	# Vectorisation de l'ensemble à évaluer
	evaluation_data_features = vectorizer.fit_transform(in_eval)
	evaluation_data_features = evaluation_data_features.toarray()		
	
	ficRes = open("resRandomForest.txt","w")
	
	clf = ensemble.RandomForestClassifier(n_estimators=10)
	clf.fit(x_train,y_train)
	predict = clf.predict(x_test)
	print(predict)
	for i in range(len(predict)):
		if(predict[i]==0):
			ficRes.write(str(i+1)+"\t nonfigurative\n")
		else:
			ficRes.write(str(i+1)+"\t figurative\n")
	#print(me.matrix_rappel(predict,y_test))
	#print(me.matrix_precision(predict,y_test))
	#print('####################################')
	
	ficRes.close()
	


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="python3 tree.py")

	parser.add_argument('corpus_train', type=argparse.FileType('r'), 
		help='Le corpus de tweets en sortie de pretraitement.py')
		
	parser.add_argument('corpus_eval', type=argparse.FileType('r'), 
	help='Le corpus de test')

	parser.add_argument('--clean', 
		help="Précise si le corpus en entrée est le résultat des prétraitements de lecture.py.", 
		action="store_true")

	__main__(vars(parser.parse_args()))




































